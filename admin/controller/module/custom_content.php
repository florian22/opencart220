<?php

class ControllerModuleCustomContent extends Controller
{
    private $error = array();

    public function index()
    {
        $this->load->language('module/custom_content');
        $this->document->setTitle($this->language->get('heading_title'));

        $this->load->model('extension/module');

        if (($this->request->server['REQUEST_METHOD'] == 'POST') && $this->validate()) {
            if (!isset($this->request->get['module_id'])) {
                $this->model_extension_module->addModule('custom_content', $this->request->post);
            } else {
                $this->model_extension_module->editModule($this->request->get['module_id'], $this->request->post);
            }

            $this->session->data['success'] = $this->language->get('text_success');

            $this->response->redirect($this->url->link('extension/module', 'token=' . $this->session->data['token'], true));
        }

        $data['heading_title'] = $this->language->get('heading_title');
        $data['button_add'] = $this->language->get('button_add');
        $data['button_cancel'] = $this->language->get('button_add');

        if (isset($this->error['warning'])) {
            $data['error_warning'] = $this->error['warning'];
        } else {
            $data['error_warning'] = '';
        }

        $data['breadcrumbs'] = array();

        $data['breadcrumbs'][] = array(
            'text' => 'Home',
            'href' => $this->url->link('common/dashboard', 'token=' . $this->session->data['token'], true)
        );

        $data['breadcrumbs'][] = array(
            'text' => 'Module',
            'href' => $this->url->link('module', 'token=' . $this->session->data['token'], true)
        );

        $data['breadcrumbs'][] = array(
            'text' => $this->language->get('heading_title'),
            'href' => $this->url->link('module/custom_content', 'token=' . $this->session->data['token'], true)
        );

        /*echo '<pre>';
        print_r($fields);
        exit();*/

        $data['add'] = $this->url->link('module/custom_content/add', 'token=' . $this->session->data['token'], true);


        $data['header'] = $this->load->controller('common/header');
        $data['column_left'] = $this->load->controller('common/column_left');
        $data['footer'] = $this->load->controller('common/footer');

        $this->response->setOutput($this->load->view('module/custom_content/custom_content', $data));
    }

    public function add() {
        $this->load->language('module/custom_content');
        $this->document->setTitle($this->language->get('heading_title'));

        $data['heading_title'] = $this->language->get('heading_title');

        if (isset($this->error['warning'])) {
            $data['error_warning'] = $this->error['warning'];
        } else {
            $data['error_warning'] = '';
        }

        if (isset($this->request->get['custom_content_id'])) {
            $data['text_form'] = $this->language->get('text_form_edit');
        } else {
            $data['text_form'] = $this->language->get('text_form_add');
        }

        $data['breadcrumbs'] = array();

        $data['breadcrumbs'][] = array(
            'text' => 'Home',
            'href' => $this->url->link('common/dashboard', 'token=' . $this->session->data['token'], true)
        );

        $data['breadcrumbs'][] = array(
            'text' => 'Module',
            'href' => $this->url->link('module', 'token=' . $this->session->data['token'], true)
        );

        $data['breadcrumbs'][] = array(
            'text' => $this->language->get('heading_title'),
            'href' => $this->url->link('module/custom_content', 'token=' . $this->session->data['token'], true)
        );

        $data['header'] = $this->load->controller('common/header');
        $data['column_left'] = $this->load->controller('common/column_left');
        $data['footer'] = $this->load->controller('common/footer');

        $this->response->setOutput($this->load->view('module/custom_content/custom_content_form', $data));
    }

    protected function validate()
    {
        if (!$this->user->hasPermission('modify', 'module/custom_content')) {
            $this->error['warning'] = $this->language->get('error_permission');
        }

        return !$this->error;
    }
}